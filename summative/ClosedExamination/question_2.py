from typing import List


def molecule_to_list(molecule: str) -> List[tuple]:
    """
    Transforms a string representing a molecule into a list of tuples.

    Args:
        molecule (str): A string containing the chemical formula for
            a molecule, which is made up of chemical element symbols
            (one upper case letter or one upper and one lower case letter)
            separated by numbers.

    Returns:
        molecule_list (list): A list of tuples, where the first element
            of each tuple corresponds to the atom, and the second corresponds
            to the number of occurrences of that atom at the given position in
            the structure of the molecule.
    """
    if molecule[0].islower():
        raise ValueError(
            "molecule does not start with an uppercase letter" +
            "from the English alphabet"
        )

    if not molecule.isalnum():
        raise ValueError(
            "molecule contains characters that are not alphanumeric"
        )

    molecule_list = []
    # Create a variable to keep track of multicharacter atoms
    atom = molecule[0]
    # Create a variable to keep track of molecule count
    # This is a string to allow for double digit numbers
    count = ""

    for i in range(1, len(molecule)):
        current = molecule[i]
        if current.isnumeric():
            count += current
        elif current.isupper():
            # If the current character is uppercase, it must be the
            # start of a new element.
            if count == "":
                count = "1"
            molecule_list.append((atom, int(count)))
            atom = current
            count = ""
        elif current.islower():
            # If the current character is lowercase, it must be the
            # 2nd character of an element, if it's not, there is an error.
            if not molecule[i - 1].isupper():
                raise ValueError(
                    "molecule has an invalid format;" +
                    "there is an unexpected lowercase character."
                )
            atom += current
        else:
            # The if statement on line 25 should have already covered this, but
            # let's put in an extra error check just in case.
            raise ValueError(
                "molecule contains characters that are not alphanumeric"
            )

    # Since a tuple is only added when the next uppercase character
    # is discovered, we need to add in the last tuple outside the loop.
    if count == "":
        count = "1"
    molecule_list.append((atom, int(count)))

    return molecule_list
