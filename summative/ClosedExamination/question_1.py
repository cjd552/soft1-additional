from typing import List


def encrypt(message: str, shifts: List[int], alphabet: str) -> str:
    """
    Encrypts a message by shifting each letter by the corresponding amount
    from an integer sequence.

    Args:
        message (str): A string containing any sequence of symbols, that is
            to be encrypted.
        shifts (list): A list of positive integers, the same length as
            message. The kth value in shifts is used to shift the kth value
            in message.
        alphabet (str): A string which defines the allowed characters in
            message. Contains one instance of each allowed character.

    Returns:
        cipher (str): The encrypted message.
    """
    if any(x not in alphabet for x in message):
        raise ValueError(
            "Message contains characters that are not in the alphabet"
        )

    if len(shifts) != len(message):
        raise ValueError(
            "The size of the shifts is not the same as the size of message"
        )

    if any(y < 0 or y >= len(alphabet) for y in shifts):
        raise ValueError(
            "shifts contains negative values or" +
            "values greater or equal to the size of the alphabet."
        )

    # If no error has been raised at this point we know that shifts and message
    # must be the same length, so only one size value is needed.
    size = len(message)
    cipher = ""
    for i in range(size):
        current = message[i]
        to_shift = shifts[i]
        # The shifted character is to_shift away from current in the alphabet;
        # so first, we need to locate the current character in the alphabet.
        current_a_index = alphabet.index(current)
        # If we reach the end of the alphabet we need to wrap back around,
        # so mod the length of the alphabet.
        shifted_a_index = (current_a_index + to_shift) % (len(alphabet))
        shifted = alphabet[shifted_a_index]
        cipher += shifted
    return cipher
