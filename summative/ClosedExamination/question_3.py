from typing import List

ATOMS = {
    "H": {"name": "Hydrogen", "weight": 1.00797},
    "He": {"name": "Helium", "weight": 4.00260},
    "C": {"name": "Carbon", "weight": 12.011},
    "N": {"name": "Nitrogen", "weight": 14.0067},
    "O": {"name": "Oxygen", "weight": 15.9994},
    "Ca": {"name": "Calium", "weight": 40.08},
}


def molar_mass(molecule: List[tuple]) -> int:
    """
    Takes a list of tuples representing a molecule and returns its molar mass.

    Args:
        molecule (list): A list of tuples, representing a molecule,
            where the first element of each tuple corresponds to the atom,
            and the second corresponds to the number of occurrences of that
            atom at the given position in the structure of the molecule.

    Returns:
        molar_mass_value (int): The molar mass of the molecule.
    """
    # A list of all the atoms in molecule with no duplicates can be obtained by
    # converting it into a dictionary and taking the keys of that dictionary.
    if any(x not in ATOMS.keys() for x in dict(molecule).keys()):
        raise ValueError("molecule contains unknown atom symbol.")

    molar_mass_value = 0.0
    for t in molecule:
        atom = t[0]
        count = t[1]
        current_mass = count * (ATOMS[atom]["weight"])
        molar_mass_value += current_mass

    return molar_mass_value
