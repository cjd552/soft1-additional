from typing import List


def check_level(level: List[int]) -> bool:
    """
    Checks whether a given game level is possible to complete.

    Args:
        level (list): A list of integers, each of which represents
            the maximum jump length from the board at that position
            in the level.

    Returns:
        A boolean value, which is True if the level is feasible,
        False otherwise.

    """
    # The "current" position is always the first element of level.
    current = level[0]
    if current < 0:
        raise ValueError("level contains a negative number")
    elif current == 0:
        return False
    elif len(level) == 1:
        # If we've reached the last position, the level is cleared.
        return True
    possible = False
    for i in range(1, current + 1):
        # attempt all possible moves from the current position
        if i < len(level):
            if check_level(level[i:]):
                # if a possible way to clear the level is found, we can
                # break the loop early and return that the level is possible.
                possible = True
                break
    return possible
