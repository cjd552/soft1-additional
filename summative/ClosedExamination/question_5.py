class Blotris:
    def __init__(self, rows: int, cols: int) -> None:
        """
        Initialise a 5x5 or larger board with zeroes.

        Args:
            rows (int): An integer greater than or equal to 5
                representing the number of rows in the board.
            cols (int): An integer greater than or equal to 5
                representing the number of columns in the board.
        """
        if rows < 5 or cols < 5:
            raise ValueError("rows and cols must be greater or equal to 5.")
        # Create a 2D list where elements are indexed [row][column]
        self._board = [[0] * cols for x in range(rows)]
        self._rows = rows
        self._cols = cols

    def add(self, shape: list, row: int, col: int) -> bool:
        """
        Adds the shape described to the board if possible.

        Args:
            shape (list): A 2D list of items representing blocks in a shape,
                where items are indexed [row][column].
                An item valued 0 means that block is empty, whist 1
                represents an occupied block.
            row (int): An integer representing the row number of the position
                where the top left corner of the shape will be placed.
            col (int): An integer representing the column number of the
                position where the top left corner of the shape
                will be placed.

        Returns:
            A boolean value, which is True if the shape was successfully
            added and False otherwise.
        """
        if row < 0 or row >= self._rows:
            return False
        if col < 0 or col >= self._cols:
            return False

        # Convert the filled coordinates in the shape to their proposed
        # coordinates on the board.
        board_shape = []
        for r in range(len(shape)):
            for c in range(len(shape[r])):
                if shape[r][c] == 1:
                    new_r = r + row
                    # Check if the position might be off the board,
                    # if so the shape cannot be placed.
                    if new_r >= self._rows:
                        return False
                    new_c = c + col
                    if new_c >= self._cols:
                        return False
                    board_shape.append({"row": new_r, "col": new_c})
        # Check if there are any filled coordinates within the
        # proposed coordinates for the shape.
        if any(self._board[x["row"]][x["col"]] == 1 for x in board_shape):
            return False
        else:
            for coord in board_shape:
                self._board[coord["row"]][coord["col"]] = 1
            return True

    def update(self) -> None:
        """
        Removes fully occupied rows and shifts down the rows accordingly.
        """
        # Create a new board with all the filled lines removed,
        # and then replace self._board with it.
        new_board = [[0] * self._cols for x in range(self._rows)]
        new_board_index = self._rows - 1
        for r in reversed(range(self._rows)):
            row = self._board[r]
            if 0 in row:
                new_board[new_board_index] = row
                new_board_index -= 1

        self._board = new_board
