# Exercise 1


def sum_pairs(integer_list: list, target: int):
    output = []
    while len(integer_list) > 0:
        current = integer_list[0]
        integer_list.remove(current)
        desired = target - current
        if desired in integer_list:
            integer_list.remove(desired)
            output.append((current, desired))
    return output


# Exercise 2


def merge(listA: list, listB: list) -> list:
    output = []
    while len(listA) != 0 and len(listB) != 0:
        left = listA[0]
        right = listB[0]
        if left <= right:
            output.append(left)
            listA.remove(left)
        else:
            output.append(right)
            listB.remove(right)
    output.extend(listA)
    output.extend(listB)
    return output


# Exercise 3


def split_text(text: str, delimiters: str) -> list:
    i = 0
    output = []
    while i < len(text):
        if text[i] in delimiters:
            if i != 0:
                output.append(text[0:i])
            text = text[i + 1 : :]
            i = -1
        i += 1
    if len(text) != 0:
        output.append(text)
    return output


def intlistify(instring: str) -> list:
    strlist = instring[1:-1:].split(", ")
    intlist = [int(x) for x in strlist]
    return intlist


if __name__ == "__main__":
    print("Exercise 1")
    ex1_strlist = input("Enter list of integers: ")
    ex1_target = input("Enter target: ")
    print(sum_pairs(integer_list=intlistify(ex1_strlist), target=int(ex1_target)))

    print("Exercise 2")
    ex2_lista = input("Enter list A")
    ex2_listb = input("Enter list B")
    print(merge(listA=intlistify(ex2_lista), listB=intlistify(ex2_listb)))

    print("Exercise 3")
    ex3_text = input("Enter text to split")
    ex3_delimiters = input("Enter delimiters")
    print(split_text(text=ex3_text, delimiters=ex3_delimiters))
