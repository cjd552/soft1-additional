def find_influencer(following: list) -> int:
    candidate = 0
    for j in range(1, len(following)):
        if 1 in following[candidate] or following[j][candidate] == 0:
            candidate = j
    if candidate == (len(following) - 1) and 1 in following[candidate]:
        return None
    else:
        for j in range(1, len(following)):
            if following[j][candidate] == 0 and j != candidate:
                return None
        return candidate


print(
    find_influencer(
        [
            [0, 1, 1, 1, 0],
            [0, 0, 1, 0, 1],
            [0, 0, 0, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 1, 0, 0],
        ]
    )
)
