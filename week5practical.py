from typing import List


# Exercise 4a - 0
def makedictionaries():
    months = {
        1: "January",
        2: "Feburary",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December",
    }
    roman_1 = {"M": 1000, "X": 10, "V": 5, "I": 1}
    periodic = {
        "H": "Hydrogen",
        "He": "Helium",
        "Li": "Lithium",
        "Be": "Beryllium",
        "B": "Boron",
        "C": "Carbon",
        "N": "Nitrogen",
    }


def gen_roman():
    roman = {}
    # I'm writing it in this way because of how the question is phrased.
    # Otherwise I would've just initialised the dictionary with those values
    roman[100000] = "T"
    roman[1000] = "M"
    roman[500] = "D"
    roman[100] = "K"
    roman[50] = "L"
    roman[10] = "X"
    roman[5] = "V"
    roman[1] = "I"
    roman[100] = "C"
    roman.pop(100000)


# Exercise 4a - 1
def display_dico(dico: dict) -> None:
    for key, val in dico.items():
        print(f"{key} -> {val}")


# Exercise 4a - 2
def concat_dico(dico1: dict, dico2: dict) -> dict:
    allkeys = list(dico1.keys()) + list(dico2.keys())
    combined = {}
    for key in allkeys:
        combined[key] = []
        for dico in (dico1, dico2):
            if key in dico.keys():
                to_add = dico[key]
                if type(to_add) == list:
                    combined[key].extend(to_add)
                else:
                    combined[key].append(to_add)
        # convert all singular items back from lists
        if len(combined[key]) == 1:
            combined[key] = combined[key][0]
    return combined


# Exercise 4a - 3
def map_list(keys: list, values: list) -> dict:
    if len(keys) != len(set(keys)):
        print("Error: Keys must be unique")
        return None
    map_dict = {}
    for i in range(len(keys)):
        map_dict[keys[i]] = values[i]
    return map_dict


# Exercise 4a - 4
def reverse_dictionary(dico: dict) -> dict:
    return map_list(list(dico.values()), list(dico.keys()))


# Exercise 4b - 1
def save_to_exo1(a_word: str) -> None:
    with open("exo1.txt", "w") as f:
        f.write(a_word)


# Exercise 4b - 2
def save_list2file(sentences: List[str], filename: str) -> None:
    with open(filename, "w") as f:
        f.writelines(sentences)


# Exercise 4b - 3
def save_to_log(entry: str, logfile: str) -> None:
    with open(logfile, "a") as f:
        f.write(entry)


# Exercise 4b - 4
def read_to_upper(filename: str) -> None:
    with open(filename, "r") as f:
        print(f.read().upper())


# Exercise 4b - 5
def to_upper_case(input_file: str, output_file: str) -> None:
    with open(input_file, "r") as inf:
        content = inf.read()
    with open(output_file, "w") as ouf:
        ouf.write(content.upper())


# Exercise 4b - 6
def sum_from_file(filename: str) -> int:
    """
    Calculates the sum of all integers contained in a file.

    Arguments:
    filename (str): the name of the file to read the integers from.

    Returns:
    intsum (int): the sum of all the input integers.
    """
    with open(filename, "r") as f:
        source = f.read()
    intstrings = source.replace("\n", " ").split(" ")
    try:
        integers = [int(x) for x in intstrings]
    except ValueError:
        raise ValueError("There are non-integers in the file.")
    intsum = sum(integers)
    return intsum


if __name__ == "__main__":
    exd0 = {"Example": 1, "Dictionary": 2}
    print("Exercise 4a - 1")
    print(f"Example dictionary: {exd0}")
    display_dico(exd0)

    print("------------------------------------------------------------")

    print("Exercise 4a - 2")
    exd1 = {"KeyOne": "ContentOne", "KeyTwo": "ContentTwoPartOne"}
    exd2 = {
        "KeyTwo": "ContentTwoPartTwo",
        "KeyThree": ["ContentThreePartOne", "ContentThreePartTwo"],
    }
    print(f"Example dictionary one: {exd1}")
    print(f"Example dictionary two: {exd2}")
    print(f"Concatenated: {concat_dico(dico1=exd1, dico2=exd2)}")

    print("------------------------------------------------------------")

    print("Exercise 4a - 3")
    exk1 = ["KeyOne", "KeyTwo", "KeyThree"]
    exv1 = ["ValOne", "ValTwo", "ValThree"]
    print(f"Example keys: {exk1}")
    print(f"Example values: {exv1}")
    print(f"Mapped dictionary: {map_list(keys=exk1, values=exv1)}")

    print("------------------------------------------------------------")

    print("Exercise 4a - 4")
    exd3 = {"KeyOne": "ValOne", "KeyTwo": "ValTwo"}
    print(f"Example dictionary: {exd3}")
    print(f"Reversed: {reverse_dictionary(dico=exd3)}")

    print("------------------------------------------------------------")

    print("Exercise 4b - 1")
    instr = input("Enter a string to save: ")
    save_to_exo1(a_word=instr)

    print("------------------------------------------------------------")

    print("Exercise 4b - 2")
    exl1 = ["Example", "List", "One"]
    exfn1 = "./week5/4b2.txt"
    print(f"Saving {exl1} to {exfn1}")
    save_list2file(sentences=exl1, filename=exfn1)

    print("------------------------------------------------------------")

    print("Exercise 4b - 3")
    inline = input("Enter a string to save: ")
    exfn2 = "./week5/4b3.txt"
    save_to_log(entry=inline, logfile=exfn2)

    print("------------------------------------------------------------")

    print("Exercise 4b - 4")
    read_to_upper(filename="./week5/4b3.txt")

    print("------------------------------------------------------------")

    print("Exercise 4b - 5")
    exfn3 = "./week5/4b5.txt"
    to_upper_case(input_file="./week5/4b3.txt", output_file="./week5/4b5.txt")

    print("------------------------------------------------------------")

    print("Exercise 4b - 6")
    print(sum_from_file(filename="./week5/4b6.txt"))
