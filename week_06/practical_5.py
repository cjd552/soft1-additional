import string

sample_text = (
    " As Python s creator, I'd like to say a few words about its "
    + "origins adding a bit of personal philosophy. "
    + "Over six years ago in December I was looking for a "
    + "hobby programming project that would keep me occupied "
    + "during the week around Christmas. My office, "
    + "a government run research lab in Amsterdam would be closed "
    + "but I had a home computer and not much else on my hands "
    + " I decided to write an interpreter for the new scripting "
    + "language  I had been thinking about lately a descendant of ABC "
    + "that would appeal to UnixC hackers I chose Python as a "
    + "working title for   the project being in a slightly irreverent "
    + "mood and a big fan of Monty Python s Flying Circus.  "
)

######################### EXERCISE 1 ##########################################


def split_text(text: str, separators: str = " ") -> list:
    i = 0
    output = []
    while i < len(text):
        if text[i] in separators:
            if i != 0:
                output.append(text[0:i])
            text = text[i + 1 : :]
            i = -1
        i += 1
    if len(text) != 0:
        output.append(text)
    return output


def get_words_frequencies(text: str) -> dict:
    words = split_text(text.casefold(), ((" " + string.punctuation)))
    freq_dict = {}
    for word in words:
        if word in freq_dict.keys():
            freq_dict[word] += 1
        else:
            freq_dict[word] = 1
    return freq_dict


def flatten(list_2d: list) -> list:
    # liststr = str(list_2d)
    # liststr = liststr.replace("[", "").replace("]", "")
    # return liststr.split(", ")
    list_1d = []
    for inner in list_2d:
        list_1d.extend(inner)
    return list_1d


def rasterise(list_1d: list, width: int) -> list:
    listlen = len(list_1d)
    if width < 1 or listlen % width != 0:
        return None
    rasterised = [list_1d[0:width]]
    for i in range(1, listlen // width):
        rasterised.append(list_1d[width * i : width * (i + 1)])
    return rasterised
