class Vector:
    def __init__(self, *data) -> None:
        """
        Creates a representation of a vector.

        Arguments:
        data (list): the data in the vector as a list, which will be stored as
        a copy in the ._vector attribute.
        """
        if len(data) == 0:
            self._vector = []
        else:
            if type(data[0]) != list:
                data = [list(data)]
            self._vector = [float(x) for x in data[0]]

    def __str__(self) -> str:
        """
        Creates a string representation of the vector.

        Returns:
        data_str (str): the vector in the format <a,b,c>
        """
        data_str = str(self._vector)
        data_str = data_str.replace("[", "<").replace("]", ">")
        return data_str

    def dim(self) -> int:
        """
        Returns the number of elements in a vector.
        """
        return len(self._vector)

    def get(self, index: int) -> float:
        """
        Returns the item at "index".
        """
        return self._vector[index]

    def set(self, index: int, value: float) -> None:
        """
        Sets the value at "index" to "value".
        """
        self._vector[index] = value

    def scalar_product(self, scalar: float) -> object:
        """
        Calculates the scalar product.
        Returns:
        product_vector: a new Vector object containing the result of
        scalar product.
        """
        product_vector = Vector([x * scalar for x in self._vector])
        return product_vector

    def add(self, other_vector: object) -> object:
        """
        Conducts vector addition between the vector and another vector provided.

        Arguments:
        other_vector (Vector): the vector to add to this vector.

        Returns:
        sum_vector (Vector): the result of vector addition.
        """
        if type(other_vector) != Vector:
            raise TypeError("Please provide a vector to add")
        elif other_vector.dim() != self.dim():
            raise ValueError("Dimensions don't match, can't add")
        else:
            sum_vector = Vector(
                [self.get(i) + other_vector.get(i) for i in range(self.dim())]
            )
            return sum_vector

    def equals(self, other_vector: object) -> bool:
        """
        Returns True if the same value is stored at each respective position in this vectorand
        another vector "other_vector", otherwise returns False

        Arguments:
        other_vector (Vector): the vector to check equivalence with

        """
        if type(other_vector) != Vector:
            raise TypeError("Please provide a vector to compare")
        else:
            return self._vector == other_vector._vector

    def __eq__(self, other_vector: object) -> bool:
        """
        Overrides == with self.equals.
        """
        try:
            return self.equals(other_vector)
        except TypeError:
            return False

    def __ne__(self, other_vector: object) -> bool:
        """
        Overrides != by returning the opposite of the self.equals result.
        """
        try:
            return not self.equals(other_vector)
        except TypeError:
            return True

    def __add__(self, other_vector: object) -> object:
        """
        Overrides + with self.add
        """
        return self.add(other_vector)

    def __getitem__(self, index: int) -> float:
        """
        Allows for vector[i]
        """
        return self.get(index)

    def __setitem__(self, index: int, value: float) -> None:
        """
        Allows for assignment to vector[i]
        """
        self.set(index, value)

    def __iadd__(self, other_vector: object) -> object:
        """
        Allows for += to add another vector to this vector and store it
        as the current vector rather than a new vector.
        """
        self._vector = self.add(other_vector)
        return self._vector

    def __rmul__(self, scalar: float) -> object:
        """
        Overrides * with self.scalar_product
        """
        return self.scalar_product(scalar)

    def __imul__(self, scalar: float) -> object:
        """
        Allows for *=
        """
        self._vector = self.scalar_product(scalar)
        return self._vector
