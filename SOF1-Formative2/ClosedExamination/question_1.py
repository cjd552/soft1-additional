def text2dictionary(filename: str) -> dict:
    airportdict = {}
    with open(filename, "r") as f:
        inlines = f.readlines()
    for line in inlines:
        line = line.replace(", ", ",").replace("\n", "")
        sline = line.split(",")
        if len(sline) != 3:
            raise IOError("There must be 3 entries in each line.")
        code = sline[2]
        if len(code) != 3:
            raise IOError("The airport code must be 3 characters")
        country = sline[1]
        if country not in airportdict.keys():
            airportdict[country] = []
        to_add = (sline[0], code)
        if to_add not in airportdict[country]:
            airportdict[country].append(to_add)
    return airportdict
