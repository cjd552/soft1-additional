class CarPark:
    def __init__(self, parklength: int) -> None:
        if parklength < 1:
            raise ValueError("Length must be greater than 1")
        self._spaces = [None for i in range(parklength)]
        self._parklength = parklength

    def get_available_spaces(self, position: int) -> int:
        tocheck = self._spaces[position:]
        counter = 0
        for space in tocheck:
            if space == None:
                counter += 1
            else:
                break
        return counter

    def park_vehicle(self, length: int, uid: int) -> int:
        if uid in self._spaces:
            raise ValueError("This car is already parked!")
        if None not in self._spaces:
            return -1
        for spacepos in range(self._parklength):
            if self._spaces[spacepos] == None:
                if self.get_available_spaces(spacepos) >= length:
                    end = length - spacepos
                    for i in range(length):
                        self._spaces[spacepos + i] = uid
                    return spacepos
        return -1

    def remove_vehicle(self, uid: int) -> bool:
        if uid not in self._spaces:
            return False
        for i in range(self._parklength):
            if self._spaces[i] == uid:
                self._spaces[i] = None
        return True
