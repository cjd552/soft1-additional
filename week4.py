from week3 import printmultiplicationtable


def sum_all(n: int):
    if n < 0:
        return -1
    else:
        return sum(range(0, n + 1))


def mul_table(n: int):
    if n < 0:
        raise ValueError(f"n should be greater than 0. You entered {n}")
    else:
        printmultiplicationtable(number=n)


def factorial(n: int):
    if n < 0:
        return -1
    else:
        return factorial(n)


def sum_digits(number: int):
    digit_list = list(str(number))
    return sum(digit_list)
