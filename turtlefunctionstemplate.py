"""
Created on 24 Jul 2020

@author: Lilian
"""

import turtle
import math

my_turtle = turtle.Turtle()
my_turtle.showturtle()

####################      WRITE YOUR CODE BELOW      #########################


def draw_triangle():
    my_turtle.penup()
    my_turtle.goto(250, -250)
    for i in range(0, 10):
        my_turtle.penup()
        multiple = i * 50
        my_turtle.goto(250 - multiple // 2, multiple // 2 - 250)
        my_turtle.pendown()
        for j in range(0, 3):
            my_turtle.left(120)
            my_turtle.forward(500 - multiple)


draw_triangle()


#################### WRITE YOUR CODE ABOVE THIS LINE #########################
####################        IGNORE CODE BELOW        #########################

## Must be the last line of code
my_turtle.screen.exitonclick()
