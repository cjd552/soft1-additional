import re


# Exercise 1
def palindrome(phrase: str):
    phrase_str = re.sub(r"[^\w]", "", phrase.casefold())
    return phrase_str == phrase_str[::-1]


# Exercise 2
def camelcase(phrase: str):
    output = ""
    for phrase in phrase.split(" "):
        output += phrase.replace(" ", "").capitalize()
    return output


def camelsplit(phrase: str):
    output = ""
    for letter in phrase:
        if letter.isupper():
            output += f" {letter}"
        else:
            output += letter
    if output[0] == " ":
        output = output[1::]
    return output


# Exercise 3
def caesar(phrase: str, shift: int, encrypt: str):
    if encrypt.casefold() == "decrypt":
        shift = int(f"-{shift}")
    phrase_str = re.sub(r"[^\w]", "", phrase.upper())
    output = ""
    for letter in phrase_str:
        ascletter = ord(letter)
        shifted = (ascletter - ord("A") + shift) % 26
        outletter = chr(ord("A") + shifted)
        output += outletter
    return output


if __name__ == "__main__":
    print("Exercise 1")
    palstr = input("Enter string to be checked for being a palindrome: ")
    print(palindrome(phrase=palstr))

    print("Exercise 2")
    camelstr = input("Enter string to convert to CamelCase and remove spaces from: ")
    print(camelcase(phrase=camelstr))
    splitstr = input("Enter CamelCase string to add spaces into: ")
    print(camelsplit(phrase=splitstr))

    print("Exercise 3")
    encryptstr = input("Enter string to be encrypted by Caesar Cypher")
    encryptshift = input("Enter amount to shift by for Caesar Cypher")
    print(caesar(phrase=encryptstr, shift=int(encryptshift), encrypt="encrypt"))
    decryptstr = input("Enter string to be decrypted by Caesar Cypher")
    decryptshift = input("Enter amount to shift by for Caesar Cypher")
    print(caesar(phrase=decryptstr, shift=int(decryptshift), encrypt="decrypt"))
