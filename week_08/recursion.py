def is_power(a: int, b: int) -> int:
    """
    A number, a, is a power of b if it is divisible by b and a/b is a power of b
    """
    if a == b:
        return True
    if a == 1:
        return not b == 0
    if b == 1:
        return False
    elif b == 0 or a % b != 0:
        return False
    else:
        return is_power(a / b, b)


def rec_sum(int_list: list) -> int:
    """
    sum() done recursively
    """
    if len(int_list) == 0:
        return 0
    if len(int_list) == 1:
        return int_list[0]
    else:
        first_int = int_list[0]
        int_list.pop(0)
        int_list[0] += first_int
        return rec_sum(int_list)


def sum_digits(number: int) -> int:
    """
    Sums the digits within an integer, recursively
    """
    if number < 0:
        number = 0 - number
    div_10 = number // 10
    if div_10 == 0:
        return number
    else:
        to_add = number - (div_10 * 10)
        return to_add + sum_digits(div_10)


def flatten(mlist: list) -> list:
    """
    turns a multi dimensional list to a single list

    Shamelessly stolen off stackoverflow because uhh research skills are essential
    """
    if mlist == []:
        return mlist
    if isinstance(mlist[0], list):
        return flatten(mlist[0]) + flatten(mlist[1:])
    return mlist[:1] + flatten(mlist[1:])


def merge(sorted_list_A: list, sorted_list_B: list) -> list:
    """
    Recursively merges lists
    """
    if len(sorted_list_A) == 0:
        return sorted_list_B
    else:
        a = sorted_list_A[0]
    if len(sorted_list_B) == 0:
        return sorted_list_A
    else:
        b = sorted_list_B[0]
    if a == b:
        return [a, b] + merge(sorted_list_A[1:], sorted_list_B[1:])
    elif a < b:
        return [a] + merge(sorted_list_A[1:], sorted_list_B)
    else:
        return [b] + merge(sorted_list_A, sorted_list_B[1:])
