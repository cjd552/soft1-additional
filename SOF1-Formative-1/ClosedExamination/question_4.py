import random


def scramble(word: str) -> str:
    """
    Scrambles an input word longer than 5 letters into a string where
    the first two and last two letters are the same as the input word,
    and the middle letters are the remaining letters from the input word in a random order.

    Args:
        word (str): The word to be scrambled

    Returns:
        scrambled (str): The word after the scrambling procedure
    """
    if len(word) <= 5:
        return word
    else:
        # get the middle section of the word, scramble it,
        # then add the original front and end back to it
        word_middle = list(word[2:-2:])
        random.shuffle(word_middle)
        str_word_middle = "".join(word_middle)
        return f"{word[0:2]}{str_word_middle}{word[-2::]}"
