def sum_integers(start: int, end: int) -> int:
    """
    Returns the sum of all integers between start and end inclusive

    Args:
        start (int): The integer to start adding from
        end (int): The integer to finish adding at

    Returns:
        int_sum (int): The sum of start, end and all integers between them.
    """

    if (end < start) or (start < 0) or (end < 0):
        return -1
    else:
        return sum(range(start, end + 1))
