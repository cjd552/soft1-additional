def create_chequerboard(n: int) -> str:
    """
    Returns a string representing a chequerboard of
    size n x n drawn with - and x characters.

    Args:
        n (int): the side length of the chequerboard

    Returns:
        board_string (str): the string representing the board
    """
    if n < 2:
        return None
    else:
        # initialise output
        board_string = ""
        # there are only two possible rows:
        # one starting with x and one starting with _
        # the string that starts with x will be made of n div 2 groups of "x-"
        # plus an extra x if n is odd
        starts_with_x = "x-" * (n // 2)
        if n % 2 == 1:
            starts_with_x += "x"
            # the string that starts with dash is the one that starts with x,
            # but with the first x removed and the end gaining
            # the opposite character to the original last character
            starts_with_dash = f"{starts_with_x[1::]}-"
        else:
            starts_with_dash = f"{starts_with_x[1::]}x"
        # now just add these strings alternately to the output
        for i in range(n):
            if i % 2 == 0:
                board_string += f"{starts_with_x}\n"
            else:
                board_string += f"{starts_with_dash}\n"
        return board_string
