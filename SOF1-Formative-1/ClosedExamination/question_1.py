def create_triangle(n: int) -> str:
    """
    Returns a string representing a triangle made out of
    - and x characters that is n levels high.

    Args:
        n (int): The number of levels for the triangle

    Returns:
        tri_string (str): The string representing the triangle
    """
    # filter out invalid n values
    if n < 0:
        return None
    elif n == 0:
        return ""
    else:
        # initialise output
        tri_string = ""
        # the number of x is the row number in the triangle
        # the number of dash is n - number of x
        # do this for each row
        for i in range(1, n + 1):
            row = f"{'x'*i}{'-'*(n-i)}\n"
            tri_string += row
        return tri_string
