def getunits(measure: str, source: str):
    units = {
        "metric": {
            "weight": "kg",
            "distance": "m",
            "liquid": "L",
        },
        "imperial": {
            "weight": "stones and pounds",
            "distance": "feet",
            "liquid": "UK pints",
        },
    }
    return units[source][measure]


def getmultiplier(destunits: str):
    return {
        "kg": 0.45359237,
        "m": 0.3048,
        "L": 0.56826125,
        "stones and pounds": 2.20462262185,
        "feet": 3.28084,
        "UK pints": 1.759753986,
    }.get(destunits)


def main():
    csource = ""
    dest = "metric"
    while csource.casefold() not in ("imperial", "metric"):
        csource = input(
            "Enter whether you'd like to convert from 'imperial' or to 'metric'"
        )

    if csource == "metric":
        dest = "imperial"

    measure = ""
    while measure.casefold() not in ("weight", "distance", "liquid"):
        measure = input(
            "Enter whether you'd like to convert 'weight', 'distance', or 'liquid' measurements."
        )

    units = getunits(measure=measure, source=csource)
    destunits = getunits(measure=measure, source=dest)

    if units == "stones and pounds":
        instones = input("Please enter number of stones")
        inpounds = input("Please enter number of pounds")
        toconvert = float(instones) * 14 + float(inpounds)
    else:
        toconvert = input(f"Please enter {measure} in {units}")

    multiplier = getmultiplier(destunits=destunits)

    converted = multiplier * float(toconvert)

    if destunits == "stones and pounds":
        outstones = converted // 14
        outpounds = converted % 14
        print(
            f"Converted {toconvert} {units} to {outstones} stones, {outpounds} pounds"
        )
    elif units == "stones and pounds":
        print(
            f"Converted {instones} stones, {inpounds} pounds to {converted} {destunits}"
        )
    else:
        print(f"Converted {toconvert} {units} to {converted} {destunits}")


if __name__ == "__main__":
    main()
