# Exercise 1
def printeven(numbers: str):
    intlist = numbers.split(" ")
    evenlist = [int(i) for i in intlist if int(i) % 2 == 0]
    evenset = set(evenlist)  # converting to set removes duplicates
    output = sorted(list(evenset))
    print(
        f"There are {len(output)} distinct even numbers: {str(output)[1:-1:].replace(',', '')}"
    )


# Exercise 2
def soduku():
    s_out = ["+-+-+-+-+"]
    for i in range(0, 4):
        checkline = False
        while checkline is False:
            line = input(
                f"Line {i}: Enter 4 digits between 0 and 4 separated by a white space: "
            )
            checkline = True
            split = line.split(" ")
            if len(split) != 4:
                print("Invalid input, please try again.")
                checkline = False
            else:
                for j in line.split(" "):
                    if 0 > int(j) or int(j) > 4:
                        checkline = False
                        print("Invalid input, please try again.")
                        break
        line = line.replace("0", " ").replace(",", "|")
        s_out.append(f"|{line}|")
        s_out.append("+-+-+-+-+")
    for out_line in s_out:
        print(out_line)


# Exercise 3
def binary():
    number = -1
    while number < 0:
        number = int(input("Enter a positive integer: "))
    quotient = number
    remainders = []
    while quotient != 0:
        remainders.insert(0, quotient % 2)
        quotient = quotient // 2
    print(
        f"The binary representation of {number} is {str(remainders)[1:-1:].replace(', ', '')}"
    )


if __name__ == "__main__":
    print("Exercise 1")
    evenin = input("Enter a series of numbers separated by spaces: ")
    printeven(numbers=evenin)

    print("Exercise 2")
    soduku()

    print("Exercise 3")
    binary()
