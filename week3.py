import pandas as pd


# Exercise 1
def askuntilnegative():
    number = 0
    lastnumbers = []
    while number >= 0:
        number = int(input("Enter a negative number: "))
        lastnumbers.append(number)
    suml = sum(lastnumbers)
    print(f"Sum: {suml}")
    print(f"Average: {suml / len(lastnumbers)}")
    print(f"Even numbers: {[i for i in lastnumbers if i % 2 == 0]}")


# Exercise 2
def printmultiplicationtable(number: int):
    tabledict = {}
    for i in range(1, 13):
        tabledict[i] = number * i
    print(f"Multiplication table of {number}")
    print(pd.DataFrame([tabledict]))


def printfactorial(number: int):
    total = 1
    for i in range(2, number + 1):
        total = total * i
    print(f"{number}! = {total}")


def askuntilpositive():
    number = 0
    while number <= 0:
        number = int(input("Enter a positive number: "))
    print(f"Sum of first {number} natural numbers: {sum(range(0, number+1))}")
    printmultiplicationtable(number=number)
    printfactorial(number=number)


if __name__ == "__main__":
    print("Exercise 1")
    askuntilnegative()
    print("Exercise 2")
    askuntilpositive()
